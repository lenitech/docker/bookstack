ARG VERSION=latest
FROM solidnerd/bookstack:$VERSION
USER root
RUN a2enmod headers
COPY expires.conf /etc/apache2/conf-available/expires.conf
RUN a2enconf expires
RUN curl -sSLf \
    -o /usr/local/bin/install-php-extensions \
    https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions opentelemetry
COPY php.ini /usr/local/etc/php/php.ini
USER 33
